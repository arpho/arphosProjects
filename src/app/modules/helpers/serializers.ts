export class Serializers{
    serialize2Boolean(value: boolean) {
        return Boolean(value)
    }

    serialize2String = (v: string) => {
      return   v ? v : ''
    }

    serialize2PositiveNumber = (n: number,defaultNumber=-1) => {
      return  n ? n : defaultNumber
    }
    serialize2Number = (n: number) => {
      return this.serialize2PositiveNumber(n,0)
    }
    serialize2Array(v:Array<any>){
      return v?v:[]

    }
}