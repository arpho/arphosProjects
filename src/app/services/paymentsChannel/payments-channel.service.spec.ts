import { TestBed } from '@angular/core/testing';

import { PaymentsChannelService } from './payments-channel.service';

describe('PaymentsChannelService', () => {
  let service: PaymentsChannelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaymentsChannelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
