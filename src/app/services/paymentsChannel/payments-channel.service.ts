import { Injectable } from '@angular/core';
import { Database, DatabaseReference, getDatabase, onValue, push, ref, remove, set } from 'firebase/database';
import { BehaviorSubject, Observable } from 'rxjs';
import { PaymentChannel } from 'src/app/models/paymentChannel';
//import { ItemModelInterface } from 'src/app/modules/item/models/itemModelInterface';
import { ItemServiceInterface } from 'src/app/modules/item/models/ItemServiceInterface';

@Injectable({
  providedIn: 'root'
})
export class PaymentsChannelService implements ItemServiceInterface{

  categoriesService?: ItemServiceInterface;
  suppliersService?: ItemServiceInterface;
  paymentsService?: ItemServiceInterface;
  reference: string= "paymentsChannels"
  _items: BehaviorSubject<PaymentChannel[]>;
  items_list: PaymentChannel[];
  db: Database;
  itemsListRef: DatabaseReference;
  items: Observable<PaymentChannel[]>;
  constructor(){}
  getItem(key: string, next: (item?: unknown) => void): void {
    const customerRef = ref(this.db, `${this.reference}/${key}`)
    onValue(customerRef, (item => {
      next(item.val())
    }),(err=>{console.error(err)}))
  }
  updateItem(item: PaymentChannel) {
    const reference = ref(this.db, `${this.reference}/${item.key}`)
   return  set(reference, item.serialize())
  }
  deleteItem(key: string) {
    const reference = ref(this.db, `${this.reference}/${key}`)
    console.log("reference",reference)
    return remove(reference).then((result)=>{
      console.log("success",result)
    })
  }
  getEmptyItem(): PaymentChannel {
    return new  PaymentChannel()
  }
  createItem(item: PaymentChannel) {
    const db = getDatabase()
    const extractionReference = ref(this.db,this.reference)
    return  push(extractionReference,item.serialize())
  }
  loadDataAndPublish(): void {
  
    const extractionReference = ref(this.db,this.reference)
    
     onValue(extractionReference,(snapShot)=>{
       this.items_list = []
       snapShot.forEach((doc)=>{
         const extraction = new PaymentChannel(doc.val()).setKey(doc.key)
         this.items_list.push(extraction)
       })
     this._items.next(this.items_list)
     },(error)=>{
       console.log("errore",error)
     })
  }

}
