import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { configs } from './configs/configs';
import { AuthGuard } from './modules/user/services/authguard';
import { ExpirationTimeGuard } from './modules/user/services/expiration-time-guard.service';
import { RoleGuardService } from './modules/user/services/role-guards.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'menu',
    pathMatch: 'full'
  },
  {
    path: 'home',
    redirectTo: 'menu',
    pathMatch: 'full'
  },

  

  {path:"users",
  
  loadChildren:()=>import('./modules/user/user.module').then(m=>m.UserModule)
},


  {
    path: 'customers',
    canActivate:[AuthGuard,RoleGuardService],
    data:{maximumRoleLevel:2,locked:configs.locked},
    loadChildren: () => import('./pages/customers/list/customers/customers.module').then( m => m.CustomersPageModule)
  },
  {
    path: 'update-customer',
    canActivate:[AuthGuard],
    data:{maximumRoleLevel:2,
    locked:configs.locked},
    loadChildren: () => import('./pages/customers/edit/update-customer/update-customer.module').then( m => m.UpdateCustomerPageModule)
  },
  {
    path: 'new-customer',
    canActivate:[AuthGuard,RoleGuardService],
    data:{maximumRoleLevel:2,locked:configs.locked},
    loadChildren: () => import('./pages/customers/create/new-customer/new-customer.module').then( m => m.NewCustomerPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'create',
    loadChildren: () => import('./pages/paymentsChannel/create/create.module').then( m => m.CreatePageModule)
  },
  {
    path: 'edit',
    loadChildren: () => import('./pages/paymentsChannel/edit/edit.module').then( m => m.EditPageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./pages/paymentsChannel/list/list.module').then( m => m.ListPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
