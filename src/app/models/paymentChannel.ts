
import { Serializers } from "../modules/helpers/serializers";
import { Genere, ItemModelInterface } from "../modules/item/models/itemModelInterface";
import { ItemServiceInterface } from "../modules/item/models/ItemServiceInterface";
import { QuickAction } from "../modules/item/models/QuickAction";
import { Value } from "../modules/item/models/value";

export class PaymentChannel implements ItemModelInterface {
    title: string;
    note?: string;
    description: string
    key: string;
    quickActions?: QuickAction[];
    archived?: boolean;
    service?: ItemServiceInterface;
    constructor(item?:{}){
        this.load(item)
    }
    getTitle(): Value {
        return new Value({ value: this.title, label: "canale di pagamento" })
    }
    getCountingText(): { singular: string; plural: string; } {
        return { singular: "canale di pagamento", plural: "canali di pagamento" }
    }
    getNote(): Value {
        return new Value({ value: this.note, label: "note" })
    }
    build?(item: {}) {
        Object.assign(this, item)
        return this
    }
    load?(item: {}, next?: () => void) {
        this.build(item)
        if (next) {
            next()
        }
    }
    isArchived?(): boolean {
        return Boolean(this.archived)
    }
    archiveItem?(b: boolean) {
        this.archived = b
    }
    isArchivable?(): boolean {
        return true
    }
    getValue2(): Value {
        return new Value({ value: this.description, label: "descrizione val2" })
    }
    getValue3(): Value {
        return new Value({ value: this.key, label: "chiave val3" })
    }
    getValue4(): Value {
        return new Value({ value: this.note, label: "val4 note" })
    }
    setKey?(key: string): PaymentChannel {
        this.key = key
        return this
    }
    getEditPopup(item?: ItemModelInterface, service?: ItemServiceInterface) {
        throw new Error("Method not implemented.");
    }
    initialize(item: {}): ItemModelInterface {
        this.build(item)
        return this
    }
    getAggregate(): Value {
        return new Value({ value: 0, label: "totale" })
    }
    hasQuickActions?(): boolean {
        return false
    }
    serialize() {
        const serializers = new Serializers()
        const out = {
            "description": serializers.serialize2String(this.description),
            "note": serializers.serialize2String(this.note),
            "archived": serializers.serialize2Boolean(this.archived),
            "title": serializers.serialize2String(this.title)
        }
        return !this.key?out:{
            "key": serializers.serialize2String(this.key),...out}
    }
    getElement(): { element: string; genere: Genere; } {
        return { element: "canale", genere: "o" }
    }

}