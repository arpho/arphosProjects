import { serialize } from "v8"
import { DateModel } from "../modules/user/models/birthDateModel"
import { WorkingTime } from "./WorkingTime"

describe("workingTime class works",()=>{
    it("instantiates correcly",()=>{
        const testDate = new DateModel(new Date())
        const testFinishingDate = new DateModel(new Date())

        const test= new WorkingTime({finishingTime:testDate.getTime()})
        expect(test.key).toBeDefined()
        expect(test.finishingTime.getTime()).toBe(testDate.getTime())
        const testWithKey = new WorkingTime({key:`${testDate.getTime()}`,finishingTime:testFinishingDate.getTime()})
        expect(Number(testWithKey.key)).toBe(testDate.getTime())

    
    })
    it("total time is calculated correctly",()=>{
        const test = new WorkingTime()
        expect(test.totalTimeInMSec).toBe(-1)
        test.setFinishingTime(Number(test.key)+5)
        expect(test.totalTimeInMSec).toBe(5)


    })
    it("serializes correctly",()=>{
        const testDate = new DateModel(new Date())
        const test= new WorkingTime({key:String(testDate.getTime()),finishingTime:testDate.getTime()})
        expect(test.serialize().key).toBe(String(testDate.getTime()))
        expect(test.serialize().finishingTime).toBe(String(testDate.getTime()))
    })
})