import { DateModel } from "../modules/user/models/birthDateModel"

export class WorkingTime {
  key: string
  _finishingTime: DateModel
  /**
   * return the diifference in ms from startingTime(key) and finishing time if defined or -1 if finishingTime is undefined
   */
  get totalTimeInMSec(): number {
    return this.finishingTime ? this._finishingTime.getTime() - new Date(Number(this.key)).getTime() : -1 //
  }
  get startingTime() {
    return new DateModel(new Date(Number(this.key)))

  }
  get finishingTime(): DateModel {
    return this._finishingTime

  }

  set finishingTime(time: DateModel) {
    this._finishingTime = time
  }/**
   * 
   * @param time :number|string datetime exprssed as milliseconds number or in american format
   * @return this
   */
  setFinishingTime(time: string | number) {
    this.finishingTime = new DateModel(new Date(Number(time)))
    return this
  }

  serialize() {
    return {
      key: this.key,
      finishingTime: String(this.finishingTime.getTime())
    }
  }
  /**
     * 
     * @param data:optional{key:string, finishingTime:string|number,} the key it is the starting Time
     */
  constructor(data?: { key?: string, finishingTime: string | number }) {
    if (data) {
      this.key = data.key ? data.key : String(new Date().getTime())
      this._finishingTime = data.finishingTime ? new DateModel(new DateModel(Number(data.finishingTime))) : undefined
    }
    else {
      this.key = String(new Date().getTime())
    }


  }
}