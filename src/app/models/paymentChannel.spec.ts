import { PaymentChannel } from "./paymentChannel"

describe("paymentChannel works", () => {

    it("instantiates correctly", () => {
        const dummy = new PaymentChannel()
        expect(dummy).toBeDefined()
        const data = {
            title: "test",
            description: "test description",
            key: "agsd",
            note: "test note",
            archived: true
        }
        const test = new PaymentChannel(data)
        expect(test.key).toBe(data.key)
        expect(test.description).toBe(data.description)
        expect(test.note).toBe(data.note)
        expect(test.title).toBe(data.title)
        expect(test.archived).toBe(data.archived)
    })
    it("serializes correctly",()=>{
        const data = {
            title: "test",
            description: "test description",
            key: "agsd",
            note: "test note",
            archived: true
        }
        const test = new PaymentChannel(data)
        expect(test.serialize()['key']).toBe(data.key)
        expect(test.serialize().description).toBe(data.description)
        expect(test.serialize().note).toBe(data.note)
        expect(test.serialize().title).toBe(data.title)
        expect(test.serialize().archived).toBe(data.archived)
        const data2={title:"test"}
        const test2 = new PaymentChannel(data2)
        expect(test2.serialize().hasOwnProperty("key")).toBeFalse()
        expect(test2.serialize().title).toBe(data2.title)
        expect(test2.serialize().note).toBe('')
        expect(test2.serialize().description).toBe('')
        expect(test2.serialize().archived).toBeFalse()

    })
    it("setters works",()=>{
        const test = new PaymentChannel()
        expect(test.isArchived()).toBeFalse()
        expect(test.setKey("abc")).toBeInstanceOf(PaymentChannel)
        expect(test.key).toBe("abc")
        test.archiveItem(true)
        expect(test.isArchived()).toBeTrue()
    })
})