import { Serializers } from "../modules/helpers/serializers";
import { Genere, ItemModelInterface } from "../modules/item/models/itemModelInterface";
import { ItemServiceInterface } from "../modules/item/models/ItemServiceInterface";
import { QuickAction } from "../modules/item/models/QuickAction";
import { Value } from "../modules/item/models/value";
import { DateModel } from "../modules/user/models/birthDateModel";
import { PaymentChannel } from "./paymentChannel";

export class Payment implements ItemModelInterface{
    title: string;
    note?: string;
    key: string;
    amount:number
    channelId:string
    _channel:PaymentChannel
    _date:DateModel
    quickActions?: QuickAction[];
    archived?: boolean;
    service?: ItemServiceInterface;
    get channel(){return this._channel}
    setChannel (c:PaymentChannel){
        this._channel=c
        this.channelId= c.key
        return this
    }/**
    @param date:date in american format
    
    */
    set date(date:string){
        this._date = new DateModel(new Date(date))
    }
    get italianDate (){
        return this._date.ItalianFormatDate()
    }
    get americanDate(){
        return this._date.formatDate()
    }
    getTitle(): Value {
       return new Value({value:this.title,label:"titolo"})
    }
    getCountingText(): { singular: string; plural: string; } {
       return {singular:"pagamento",plural:"plurale"}
    }
    getNote(): Value {
       return new Value({value:this.note,label:"note"})
    }
    build?(item: {}) {
        return this.initialize(item)
        
    }
    load?(next?: () => void) {
        throw new Error("Method not implemented.");
    }
    isArchived?(): boolean {
       return this.archived
    }
    archiveItem?(b: boolean) {
        this.archived=b
    }
    isArchivable?(): boolean {
        return true
    }
    getValue2(): Value {
        return new Value({value:this.amount,label:"cifra corrisposta"})
    }
    getValue3(): Value {
        return new Value({value:this.key,label:"id"})
    }
    getValue4(): Value {
        return new Value({value:this.italianDate,label:"data di pagamento"})
    }
    getEditPopup(item?: ItemModelInterface, service?: ItemServiceInterface) {
        throw new Error("Method not implemented.");
    }
    initialize(item: {key?:string}): ItemModelInterface {
        Object.assign(item,this)
        if(!item.key){
            this.key = String(new Date().getTime())
        }
        return this
    }
    getAggregate(): Value {
        throw new Error("Method not implemented.");
    }
    aggregateAction?() {
        throw new Error("Method not implemented.");
    }
    hasQuickActions?(): boolean {
        return false
    }
    serialize() {
        const serializers = new Serializers()
        return {
            key:serializers.serialize2String( this.key),
            amount:serializers.serialize2Number(this.amount),
            archived:serializers.serialize2Boolean(this.archived),
            title:serializers.serialize2String(this.title),
            note:serializers.serialize2String(this.note),
            date:serializers.serialize2String(this.americanDate),
            PaymentChannel:serializers.serialize2String(this.channelId)
        }
    }
    getElement(): { element: string; genere: Genere; } {
        return {element:"pagamento",genere:'o'}
    }
    
}